//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Abubakr Khabebulloev on 2021/09/30.
//

import SwiftUI

@main
struct LandmarksApp: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
