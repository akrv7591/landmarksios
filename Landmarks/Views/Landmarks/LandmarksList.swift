//
//  LandmarksList.swift
//  Landmarks
//
//  Created by Abubakr Khabebulloev on 2021/09/30.
//

import SwiftUI

//let devices = ["iPhone 11", "iPhone SE (2nd generation)", "iPhone XS Max"]
let devices = ["iPhone 11"]

struct LandmarksList: View {
    @EnvironmentObject var modelData: ModelData
    @State private var showFavoriteOnly = false
    
    var filteredLandmarks: [Landmark] {
        modelData.landmarks.filter { landmark in
            (!showFavoriteOnly || landmark.isFavorite)
        }
    }
    
    var body: some View {
        NavigationView {
            List{
                Toggle(isOn: $showFavoriteOnly) {
                    Text("Favorites only")
                }
                
                ForEach (filteredLandmarks) { landmark in
                    NavigationLink(destination: LandmarkDetail(landmark: landmark)) {
                        LandmarkRow(landmark: landmark)
                    }
                    
                }
            }
            .navigationTitle("Landmarks")
        }
        
    }
}

struct LandmarksList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            LandmarksList()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .environmentObject(ModelData())
        }
    }
}
